#!/bin/bash

cd /usr/local/bin/node-red && \
npm install --unsafe-perm --no-update-notifier --no-audit --no-fund --only=production

