# node-red-alpine

Installskripts to install nodered on alpine linux.


1. Pre Installskripts

    wget -O - https://gitlab.com/thomas.plorin/node-red-alpine/-/raw/master/1_prerequisite.bash?inline=false | /bin/sh

2. Change to /tmp/node-red-alpine

    cd /tmp/node-red-alpine

3. Change to bash

    bash

3. Run Installation

    ./2_install.bash



