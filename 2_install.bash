#!/bin/bash

if [ ! -f "/bin/bash" ]; then
	echo "Please add bash"
	echo "apk add bash"
	exit 1
fi


mkdir -p /usr/local/bin/node-red && \
  cp -R src/usr/local/bin/node-red/* /usr/local/bin/node-red/ && \
  adduser -h /usr/local/bin/node-red -D -H node-red -u 1000 && \
  chown -R node-red:root /usr/local/bin/node-red && \
  chmod -R g+rwX /usr/local/bin/node-red && \
  cd /usr/local/bin/node-red && \
  apk add --no-cache \
      bash \
      tzdata \
      iputils \
      curl \
      nano \
      git \
      openssl \
      openssh-client \
      nodejs \
      npm \
      python3 \
      py3-pip \
      mosquitto && \
  rc-update add mosquitto default && \
  apk add --no-cache --virtual devtools build-base linux-headers udev && \
  su node-red -c /tmp/node-red-alpine/3_install-node-red.bash && \
  npm install -g pm2 && \
  pm2 start "/usr/local/bin/node-red/node-red.bash -- -v" && \
  pm2 save && \
  pm2 startup && \
  printf "\n\nInstallation complete\n\n" && \
  printf "\n\nPlease Reboot and check Services (node-red/mosquitto)\n\n" && \
  exit

printf "\n\nInstallation complete with error(s)\n\n"

